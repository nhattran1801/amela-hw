//cau 7: tính tuổi theo date
//calculate_age(new Date ( 1982,11,4))
const calculateAge = (birthDate) => {
  const currentDate = new Date();

  const age = currentDate.getFullYear() - birthDate.getFullYear();
  const isBirthdayCome =
    currentDate.getMonth() >= birthDate.getMonth() &&
    currentDate.getDate() >= birthDate.getDate();

  return isBirthdayCome ? age : age - 1;
};
console.log(calculateAge(new Date(1982, 11, 4)));
