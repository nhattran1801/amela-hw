//câu 2: lấy số ngày trong tháng
const getDaysInMonth = (month, year) => {
  return new Date(year, month + 1, 0).getDate();
};

console.log(getDaysInMonth(4, 2024));
