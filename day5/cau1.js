// câu 1 :lấy ngày hiện tại, truyền separator để phân tách

const getCurrentDate = (separator) => {
  const date = new Date();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const year = date.getFullYear();
  const formattedDate = [day, month, year].join(separator);

  return formattedDate;
};

console.log(getCurrentDate("/"));
console.log(getCurrentDate("-"));
