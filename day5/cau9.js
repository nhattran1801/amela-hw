// //cau 9: đếm ngc đến Tết dương

const countdownToNewYear = () => {
  const currentDate = new Date();
  const nextNewYear = new Date(currentDate.getFullYear(), 11, 31, 23, 59, 59);
  //month start from 0
  const timeTillNewYear = nextNewYear - currentDate;

  const days = Math.floor(timeTillNewYear / 1000 / 60 / 60 / 24);

  const hours = Math.floor(timeTillNewYear / 1000 / 60 / 60) % 24;
  const minutes = Math.floor(timeTillNewYear / 1000 / 60) % 60;
  const seconds = Math.floor(timeTillNewYear / 1000) % 60;
  return `${days} days, ${hours} hours, ${minutes} minutes, ${seconds} seconds`;
};
console.log(countdownToNewYear());
