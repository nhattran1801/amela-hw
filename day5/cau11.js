//câu 11: viết hàm rs data, input là obj,output là obj sau khi dc reset
// number => 0, string => "", boolean => "false"

const resetData = (inputObj) => {
  const resetObj = {};

  for (const key in inputObj) {
    switch (typeof inputObj[key]) {
      case "number":
        resetObj[key] = 0;
        break;
      case "boolean":
        resetObj[key] = false;
        break;
      case "string":
        resetObj[key] = "";
        break;
      case "Array.isArray(inputObj[key])":
        resetObj[key] = inputObj[key].map((value) =>
          typeof value === "string" ? "" : 0
        );
        break;

      case "object":
        if (inputObj[key] !== null) {
          resetObj[key] = resetData(inputObj[key]);
        } else {
          resetObj[key] = null;
        }
        break;
      default:
        resetObj[key] = inputObj[key];
    }
  }

  return resetObj;
};

const inputObj = {
  num: 10,
  str: "a",
  numArr: [1, 2, 3],
  strArr: ["a", "b", "c"],
  boolean: true,
  obj: {
    numObj: 5,
    strObj: "b",
    arrStr: ["a", "b"],
    obj2: {
      arrStr2: ["c", "d"],
      arrNum2: [1, 2, 3, 4],
    },
  },
};

console.log(resetData(inputObj));
