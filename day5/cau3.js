// caau3: check xem nay có phải cuối tuần ko
const isWeekend = (date) => {
  const dayOfWeek = date.getDay();
  // Sunday - Saturday : 0 - 6
  return dayOfWeek === 0 || dayOfWeek === 6 || dayOfWeek === 5;
};

const today = new Date();
const someDay = new Date("April 10, 2024");
console.log(isWeekend(someDay));
