//cau10: tham số 1 là chuỗi time dạng "giờ:phut:giây", tham số t2 là x
//  ví dụ: t = "9:20:56" và x = 7, =>kq "9:21:3"

const addSeconds = (timeString, x) => {
  const [hours, minutes, seconds] = timeString.split(":");

  const totalSeconds =
    Number(hours) * 60 * 60 + Number(minutes) * 60 + Number(seconds) + x;

  const newHours = Math.floor(totalSeconds / 3600);
  const newMinutes = Math.floor((totalSeconds % 3600) / 60);
  const newSeconds = totalSeconds % 60;

  const newTimeString = `${newHours}:${newMinutes
    .toString()
    .padStart(2, "0")}:${newSeconds.toString().padStart(2, "0")}`;

  return newTimeString;
};

const time = "9:20:56";
const x = 7;
console.log(addSeconds(time, x)); //"9:21:03"
