//cau 8: lấy ngày kết thúc tháng

const getLastDayOfMonth = (year, month) => {
  const lastDayOfMonth = new Date(year, month, 0);
  return lastDayOfMonth.getDate();
};

console.log(getLastDayOfMonth(2024, 4));
