// caau5: đếm ngày trôi qua từ đầu năm

const daysPassed = () => {
  const currentDate = new Date();

  const beginOfYear = new Date(currentDate.getFullYear(), 0, 1);

  const timePassed = currentDate - beginOfYear;
  // 1000 milliseconds = 1 sec
  const daysPassed = Math.floor(timePassed / (1000 * 60 * 60 * 24));

  return daysPassed;
};

console.log(daysPassed());
