//cau 4 trả về số phút và giờ
const minutesToHour = (minute) => {
  const hours = Math.floor(minute / 60);
  const minutes = minute % 60;

  if (minutes === 0) {
    return hours + " hours";
  } else {
    return hours + " hours and " + minutes + " minutes";
  }
};

console.log(minutesToHour(70));
