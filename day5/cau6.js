//caau6 : lấy ngày bđ của đầu tuần
// let dt= new Date()
//startOfWeek(dt)

const getStartOfWeek = (date) => {
  const dayOfWeek = date.getDay();
  console.log("dayOfWeek", dayOfWeek);

  const startOfWeek = new Date(date);
  startOfWeek.setDate(date.getDate() - dayOfWeek);

  return startOfWeek;
};
console.log(getStartOfWeek(new Date(2024, 4, 4)));
