// c4: tinh tong so chia het cho 5 tu 0 -> 100

const divideFive = () => {
  let sum = 0;
  for (let i = 5; i <= 100; i += 5) {
    if (i % 5 == 0) {
      sum += i;
    }
  }
  return sum;
};
console.log(divideFive());
