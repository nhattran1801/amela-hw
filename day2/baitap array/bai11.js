// c11: tra ve tap hop con cua 1 chuoi~
// sub_String("dog") => ["d","do","dog","o","og","g"]

const subString = (str) => {
  const newStr = [];
  for (let i = 0; i < str.length; i++) {
    for (let j = i + 1; j <= str.length; j++) {
      newStr.push(str.substring(i, j));
    }
  }
  return newStr;
};

console.log(subString("dog"));
