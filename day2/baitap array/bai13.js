// c13: kt xem arr co phai la arr so' le? giam dan ko
// const isOddDecrease = (arr) => {
//   for (let i = 0; i < arr.length - 1; i++) {
//     if (arr[i] % 2 === 0) {
//       return false;
//     }
//     if (arr[i] <= arr[i + 1]) {
//       return false;
//     }
//   }
//   return true;
// };

const isOddDecrease = (arr) => {
  return arr.every((value, index) => {
    if (index > 0 && value >= arr[index - 1]) {
      return false;
    }
    return value % 2 !== 0;
  });
};
console.log(isOddDecrease([5, 3, 1]));
console.log(isOddDecrease([5, 4, 3, 9, 1]));
