// c10 lấy phần từ ko xuất hiện ở cả 2 mảng

const uniqueArr = (arr1, arr2) => {
  // let result = []
  // let intersection = arr1.filter(x => !arr2.includes(x));
  // let intersection2 = arr2.filter(x => !arr1.includes(x));
  // return result[intersection,intersection2].flat(9);

  let result = arr1
    .filter((x) => !arr2.includes(x))
    .concat(arr2.filter((x) => !arr1.includes(x)));
  return result;
};
console.log(uniqueArr([1, 2, 3], [1, 2, 4]));
