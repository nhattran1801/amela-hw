// c8: doi cho ngau nhien tung phan tu trong arr
const randomShuffle = (arr) => {
  return arr.sort(() => Math.random() - 0.5);
};
console.log(randomShuffle([1, 2, 3, 4, 5, 6]));
