//c5:Viết hàm cho phép truyền ào 1 mảng các số, kq trả về
//là 1 mảng mới với các số là số dư tương ứng khi chia mảng cũ cho 2

const remainderArray = (arr) => {
  const resultArray = [];
  for (let i = 0; i < arr.length; i++) {
    const remainder = arr[i] % 2;
    resultArray.push(remainder);
  }
  return resultArray;
};
console.log(remainderArray([1, 2, 3, 4, 5]));
