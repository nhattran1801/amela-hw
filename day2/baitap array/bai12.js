// c12: kiem tra xem arr co phai tang dan` ko

const isIncrease = (arr) => {
  return arr.every((value, index) => index === 0 || value > arr[index - 1]);
};

console.log(isIncrease([1, 2, 3])); //true
console.log(isIncrease([5, 4, 3, 9, 1]));

// const isIncrease = (arr) => {
//   for (let i = 0; i < arr.length - 1; i++) {
//     if (arr[i] > arr[i + 1]) {
//       return false;
//     }
//   }
//   return true;
// };
