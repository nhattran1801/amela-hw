//c6 cho arr of string,lay ra cac phan tu co length lon nhat
const longestElement = (arr) => {
  const maxLength = Math.max.apply(
    null,
    arr.map((str) => str.length)
  );
  // console.log(maxLength);

  const filterArr = arr.filter((str) => str.length === maxLength);
  return filterArr;
};
console.log(longestElement(["aba", "aa", "ad", "c", "vcd"]));
