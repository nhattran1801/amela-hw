//c2: find second biggest num in arr

const secondBiggest = (arr) => {
  const newArr = arr.sort((a, b) => a - b);
  return newArr[1];
};
console.log(secondBiggest([1, 2, 3, 4, 5]));
