
// c5: cho mảng các user, mỗi user có cấu trúc

// user = {
//   name: string,
//   age: number,
//   isStatus: boolean
// };
// lấy ra user có age > 25 và isStatus = true

const filterUser = (users) => {
  return users.filter((user) => user.age > 25 && user.isStatus);
};

const users = [
  { name: "A", age: 33, isStatus: true },
  { name: "B", age: 22, isStatus: false },
  { name: "C", age: 11, isStatus: true }
];

console.log(filterUser(users));
