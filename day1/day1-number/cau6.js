// c6: cho 1 số nguyên, sắp xếp lại sao cho ra 1 số nhỏ nhất, ko tính số 0 đầu tiên
const sortNum = (num) => {
  let newStr = String(num);
  return newStr
    .split("")
    .sort((a, b) => a - b)
    .join("");
};

console.log(sortNum(101));
