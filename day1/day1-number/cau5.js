// c5: tinh tong tat ca cac' uoc' cua so do
const sumOfDiv = (num) => {
  let sum = 0;
  for (let i = 2; i < Math.sqrt(num); i++) {
    if (num % i === 0) {
      sum += i;
    }
  }
  return sum;
};
