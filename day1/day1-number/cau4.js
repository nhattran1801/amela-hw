// c4 : tinh tong cac so nguyen to <= tham so truyen vao
const checkPrime = (num) => {
  if (num == 2 || num == 3) {
    return true;
  }
  for (let i = 2; i <= Math.sqrt(num); i++) {
    if (num % i === 0) {
      return false;
    }
    return true;
  }
};

console.log(checkPrime(4));

const sumPrime = (num) => {
  let sum = 0;
  for (let i = 2; i < num; i++) {
    if (checkPrime(i)) {
      sum += i;
    }
    return sum;
  }
};
