// c2 tinh tong so nguyen nam giua 2 so
const sumOfNum = (num1, num2) => {
  if (num1 > num2) {
    [num1, num2] = [num2, num1];
  }
  let sum = 0;
  for (let i = num1 + 1; i < num2; i++) {
    sum += i;
  }
  return sum;
};
console.log(sumOfNum(4, 1)); //
