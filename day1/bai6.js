// bai 6: viết hàm đảo ngược chuỗi

const revertStr = (str) => {
  return str.split("").reverse().join("");
};
console.log(revertStr("Hello")); // olleh
