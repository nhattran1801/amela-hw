// bai 8 (check if uppercase or not)

const checkUpperCase = (str) => {
  for (let i = 0; i < str.length; i++) {
    if (str[i] !== str[i].toUpperCase()) {
      return false;
    }
  }
  return true;
};
console.log(checkUpperCase("HEaLO")); //
