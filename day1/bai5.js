// bai 5: cho 1 str và số nguyên n >1 , viết hàm có tác dụng sao chép chuỗi đó lên n lần, ngăn cách bởi dấu gạch ngang
const copyStrnTimes = (str, num) => {
  let result = "";
  for (let i = 0; i < num; i++) {
    if (i > 0) {
      result += "-";
    }
    result += str;
  }
  return result;
};
console.log(copyStrnTimes("a", 4)); // aaaaaaaaa
